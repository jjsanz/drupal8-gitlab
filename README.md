# GitLab CI sample integration with Drupal 8

[![pipeline status](https://gitlab.com/juampynr/drupal8-gitlab/badges/master/pipeline.svg)](https://gitlab.com/juampynr/drupal8-gitlab/commits/master) ![coverage](https://gitlab.com/juampynr/drupal8-gitlab/badges/master/coverage.svg?job=drupal8ci:code_coverage)

This repository contains a sample Drupal 8 project connected to GitLab CI. When a pull
request is created against this repository, GitLab runs the following jobs against it:

![CircleCI jobs](docs/images/pipeline.png)

If you want to install this into your project, follow instructions at the
[GitLab CI installer for Drupal 8](https://github.com/Lullabot/drupal8ci#gitlab-ci).

## Database installation
The database used by some of the jobs is built by a Dockerfile. Steps on how to
build an image with your project's database can be found at [scripts/database](scripts/database).
